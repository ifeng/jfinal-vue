package com.ljph.seckill.service;

import com.jfinal.aop.Enhancer;
import com.ljph.seckill.common.BaseTest;
import com.ljph.seckill.dto.Exposer;
import com.ljph.seckill.dto.SeckillExecution;
import com.ljph.seckill.exception.SeckillException;
import com.ljph.seckill.model.Seckill;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by yuzhou on 16/8/27.
 */
public class SeckillServiceTest extends BaseTest {

    private static final Logger _log = LoggerFactory.getLogger(SeckillServiceTest.class);

    @Before
    public void setUp() throws Exception {
        _log.info("set up");
        start();
    }

    @After
    public void tearDown() throws Exception {
        _log.info("tear down");
        stop();
    }

    @Test
    public void testGetSeckillList() throws Exception {
        _log.info("testGetSeckillList");
        SeckillService service = Enhancer.enhance(SeckillService.class);
        List<Seckill> list = service.getSeckillList();
        _log.info("list = {}", list);
    }

    @Test
    public void testGetById() throws Exception {
        SeckillService service = Enhancer.enhance(SeckillService.class);
        long id = 1000L;
        Seckill seckill = service.getById(id);

        _log.info("seckill = {}", seckill);
    }

    @Test
    public void testExportSeckillUrl() throws Exception {
        SeckillService service = Enhancer.enhance(SeckillService.class);
        Exposer exposer = service.exportSeckillUrl(1000L);
        _log.info("exposer = {}", exposer);

        if(exposer.isExposed()) {
            SeckillExecution seckillExecution = service.executeSeckill(1000L, 13776573631L, "dfdfdfdf");
            _log.info("seckillExecution = {}", seckillExecution);
        }
    }

    @Test
    public void testExecuteSeckill() throws Exception {

        SeckillService service = Enhancer.enhance(SeckillService.class);

        try {
            SeckillExecution seckillExecution = service.executeSeckill(1000L, 13776573631L, "dfdfdfdf");
            _log.info("seckillExecution = {}", seckillExecution);
        } catch (SeckillException e) {
            _log.error("Error: ", e);
        }
    }
}