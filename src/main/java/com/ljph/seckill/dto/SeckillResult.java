package com.ljph.seckill.dto;

/**
 * 封装JSON结果
 * Created by yuzhou on 16/9/1.
 */
public class SeckillResult<T> {

    private boolean success;

    private T data = null;

    private String error = null;

    public SeckillResult(boolean success, T data) {
        this.success = success;
        this.data = data;
    }

    public SeckillResult(boolean success, T data, String error) {
        this.success = success;
        this.data = data;
        this.error = error;
    }

    public SeckillResult(boolean success) {
        this.success = success;
    }

    public SeckillResult(T data) {
        this.success = true;
        this.data = data;
    }

    public SeckillResult(String error) {
        this.success = false;
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
